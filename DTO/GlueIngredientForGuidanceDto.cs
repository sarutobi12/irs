﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INK_API.DTO
{
    public class GlueIngredientForGuidanceDto
    {
        public int ID { get; set; }
        public int GlueID { get; set; }
        public string Real { get; set; }
    }
}
