﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INK_API.DTO
{
    public class QrPrintDto
    {
        public int ID { get; set; }
        public DateTime ManufacturingDate { get; set; }
    }
}
