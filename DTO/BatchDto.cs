﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INK_API.DTO
{
    public class BatchDto
    {
        public int ID { get; set; }
        public string BatchName { get; set; }
    }
}
