using System.Threading.Tasks;
using INK_API._Repositories.Interface;
using INK_API.Data;
using INK_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using INK_API.DTO;
using System.Collections.Generic;
using AutoMapper;

namespace INK_API._Repositories.Repositories
{
    public class MixingRepository : IoTRepository<Mixing>, IMixingRepository
    {
        private readonly IoTContext _context;

        public MixingRepository(IoTContext context) : base(context)
        {
            _context = context;
        }
    }
}