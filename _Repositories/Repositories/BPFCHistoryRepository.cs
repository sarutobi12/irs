using System.Threading.Tasks;
using INK_API._Repositories.Interface;
using INK_API.Data;
using INK_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using INK_API.DTO;
using System.Collections.Generic;

namespace INK_API._Repositories.Repositories
{
    public class BPFCHistoryRepository : ECRepository<BPFCHistory>, IBPFCHistoryRepository
    {
        private readonly DataContext _context;
        public BPFCHistoryRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<bool> CheckGlueID(int code)
        {
            return await _context.BPFCHistories.AnyAsync(x => x.GlueID.Equals(code));
        }



        //Login khi them repo
    }
}