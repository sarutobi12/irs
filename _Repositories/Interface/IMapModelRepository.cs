using System.Collections.Generic;
using System.Threading.Tasks;
using INK_API.Data;
using INK_API.DTO;
using INK_API.Models;

namespace INK_API._Repositories.Interface
{
    public interface IMapModelRepository : IECRepository<MapModel>
    {
      

    }
}