using System.Threading.Tasks;
using INK_API.Data;
using INK_API.Models;

namespace INK_API._Repositories.Interface
{
    public interface IIngredientInfoReportRepository : IECRepository<IngredientInfoReport>
    {
        Task<bool> CheckBarCodeExists(string code);
        Task<bool> CheckExists(int id);
        //viet them ham o day neu chua co trong ECRepository
    }
}