using INK_API.Data;
using INK_API.Models;

namespace INK_API._Repositories.Interface
{
   public interface IMixingRepository : IIoTRepository<Mixing>
    {
    }
}