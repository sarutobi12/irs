using System.Threading.Tasks;
using INK_API.Data;
using INK_API.Models;

namespace INK_API._Repositories.Interface
{
    public interface IGlueRepository : IECRepository<Glue>
    {
        bool Save();
        Task<bool> CheckExists(int id);
        Task<bool> CheckBarCodeExists(string code);
        //viet them ham o day neu chua co trong ECRepository
    }
}