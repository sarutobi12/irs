﻿using INK_API.Data;
using INK_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INK_API._Repositories.Interface
{
    public interface IPartName2Repository : IECRepository<PartName2>
    {
    }
}
