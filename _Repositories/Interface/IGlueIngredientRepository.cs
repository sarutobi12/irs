using System.Collections.Generic;
using System.Threading.Tasks;
using INK_API.Data;
using INK_API.DTO;
using INK_API.Models;

namespace INK_API._Repositories.Interface
{
    public interface IGlueIngredientRepository : IECRepository<GlueIngredient>
    {
        Task<object> GetIngredientOfGlue(int glueid);
        //viet them ham o day neu chua co trong ECRepository
        Task<bool> EditPercentage(int glueid, int ingredientid, int percentage);
        Task<bool> EditAllow(int glueid, int ingredientid, int allow);
        Task<Glue> Guidance(List<GlueIngredientForGuidanceDto> glueIngredientForGuidanceDto);

    }
}