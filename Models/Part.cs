﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace INK_API.Models
{
    public class Part
    {
       
        public int ID { get; set; }
        public string Name { get; set; }
        public int ObjectID { get; set; }
        public int ProcessID { get; set; }
        public InkTblObject InkTblObject { get; set; }
        // public ICollection<Schedules> Schedules { get; set; }
    }
}
