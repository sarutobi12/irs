using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using INK_API.Helpers;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using INK_API._Repositories.Interface;
using INK_API._Services.Interface;
using INK_API.DTO;
using INK_API.Models;
using Microsoft.EntityFrameworkCore;

namespace INK_API._Services.Services
{
    public class PartService : IPartService
    {
        private readonly IPartRepository _repoPart;
        private readonly IScheduleRepository _repoSchedule;
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        public PartService(IScheduleRepository repoSchedule, IPartRepository repoPart, IMapper mapper, MapperConfiguration configMapper)
        {
            _configMapper = configMapper;
            _mapper = mapper;
            _repoPart = repoPart;
            _repoSchedule = repoSchedule;

        }

        public async Task<bool> Add(PartDto model)
        {
            var part = _mapper.Map<Part>(model);
            _repoPart.Add(part);
            await _repoPart.SaveAll();

            var schedule = _repoSchedule.FindById(model.scheduleID);
            schedule.ID = 0 ;
            schedule.PartID = part.ID;
            _repoSchedule.Add(schedule);
            
            return await _repoSchedule.SaveAll();
        }

        public async Task<bool> Deletes(int id)
        {
            var part = _repoPart.FindById(id);
            _repoPart.Remove(part);
            var schedule = _repoSchedule.FindAll().FirstOrDefault(x => x.PartID == id);
            _repoSchedule.Remove(schedule);
            return await _repoSchedule.SaveAll();
        }

        public Task<bool> Delete(object id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<PartDto>> GetAllAsync()
        {
            var lists = await _repoPart.FindAll().ProjectTo<PartDto>(_configMapper).OrderBy(x => x.Name).ToListAsync();
            return lists;
        }

        public PartDto GetById(object id)
        {
            throw new NotImplementedException();
        }

        public Task<PagedList<PartDto>> GetWithPaginations(PaginationParams param)
        {
            throw new NotImplementedException();
        }

        public Task<PagedList<PartDto>> Search(PaginationParams param, object text)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Update(PartDto model)
        {
            var part = _mapper.Map<Part>(model);
            _repoPart.Update(part);
            return await _repoPart.SaveAll();
        }
    }
}