using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using INK_API.Helpers;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using INK_API._Repositories.Interface;
using INK_API._Services.Interface;
using INK_API.DTO;
using INK_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Transactions;

namespace INK_API._Services.Services
{
    public class ScheduleService : IScheduleService
    {
        private readonly IKindRepository _repoLine;
        private readonly IProcessRepository _repoProcess;
        private readonly IChemicalRepository _repoChemical;
        private readonly IScheduleRepository _repoSchedule;
        private readonly ISupplierRepository _repoSup;
        private readonly IInkRepository _repoInk;
        private readonly IModelNameRepository _repoModelName;
        private readonly IModelNoRepository _repoModelNo;
        private readonly IArticleNoRepository _repoArticleNo;
        private readonly IArtProcessRepository _repoArtProcess;
        private readonly IInkObjectRepository _repoObject;
        private readonly IPartRepository _repoPart;

        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        public ScheduleService(
            IModelNameRepository repoModelName,
            IModelNoRepository repoModelNo,
            IArticleNoRepository repoArticleNo,
            IArtProcessRepository repoArtProcess,
            IPartRepository repoPart,
            IInkObjectRepository repoObject,
            IScheduleRepository repoSchedule, 
            IChemicalRepository repoChemical, 
            IProcessRepository repoProcess, 
            ISupplierRepository repoSup,
            IInkRepository repoInk,
            IKindRepository repoBrand,
            IMapper mapper,
            MapperConfiguration configMapper)
        {
            _configMapper = configMapper;
            _mapper = mapper;
            _repoLine = repoBrand;
            _repoInk = repoInk;
            _repoSup = repoSup;
            _repoProcess = repoProcess;
            _repoChemical = repoChemical;
            _repoSchedule = repoSchedule;
            _repoObject = repoObject;
            _repoPart = repoPart;
            _repoModelName = repoModelName;
            _repoModelNo = repoModelNo;
            _repoArticleNo = repoArticleNo;
            _repoArtProcess = repoArtProcess;

        }

        private async Task<ScheduleDto> AddSchedule(ScheduleDtoForImportExcel scheduleDto)
        {
            var result = new ScheduleDto();
            result.ProductionDate = scheduleDto.ProductionDate;
            // make model name, model no, article no, process
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
             new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }, TransactionScopeAsyncFlowOption.Enabled))
            {

                // make model name
                var process = await _repoProcess.FindAll().FirstOrDefaultAsync(x => x.Name.ToUpper().Equals(scheduleDto.Process.ToUpper()));
                if (process != null)
                {
                    result.ProcessID = process.ID;
                } 

                // make model name
                var modelName = await _repoModelName.FindAll().FirstOrDefaultAsync(x => x.Name.ToUpper().Equals(scheduleDto.ModelName.ToUpper()));
                if (modelName != null)
                {
                    result.ModelNameID = modelName.ID;
                }
                else
                {
                    var modelNameModel = new ModelName { Name = scheduleDto.ModelName };
                    _repoModelName.Add(modelNameModel);
                    await _repoModelNo.SaveAll();
                    result.ModelNameID = modelNameModel.ID;
                }
                // end make model no

                // Make model no
                var modelNo = await _repoModelNo.FindAll().FirstOrDefaultAsync(x => x.Name.ToUpper().Equals(scheduleDto.ModelNo.ToUpper()) && x.ModelNameID == result.ModelNameID);
                if (modelNo != null)
                {
                    result.ModelNoID = modelNo.ID;
                }
                else
                {
                    var modelNoModel = new ModelNo { Name = scheduleDto.ModelNo, ModelNameID = result.ModelNameID };
                    _repoModelNo.Add(modelNoModel);
                    await _repoModelNo.SaveAll();
                    result.ModelNoID = modelNoModel.ID;
                }
                // end make model NO

                // end make articleNO

                var artNo = await _repoArticleNo.FindAll().FirstOrDefaultAsync(x => x.Name.ToUpper().Equals(scheduleDto.ArticleNo.ToUpper()) && x.ModelNoID == result.ModelNoID);
                if (artNo != null)
                {
                    result.ArticleNoID = artNo.ID;
                }
                else
                {
                    // make art no
                    var articleNoModel = new ArticleNo { Name = scheduleDto.ArticleNo, ModelNoID = result.ModelNoID };
                    _repoArticleNo.Add(articleNoModel);
                    await _repoArticleNo.SaveAll();
                    result.ArticleNoID = articleNoModel.ID;
                }
                // end articleNO

                //  make Art Process

                var artProcess = await _repoArtProcess.FindAll().FirstOrDefaultAsync(x => x.ProcessID == process.ID && x.ArticleNoID == result.ArticleNoID);
                if (artProcess != null)
                {
                    result.ArtProcessID = artProcess.ID;
                }
                else
                {
                    // make art process
                    var artProcessModel = new ArtProcess { ArticleNoID = result.ArticleNoID, ProcessID = process.ID };
                    _repoArtProcess.Add(artProcessModel);
                    await _repoArtProcess.SaveAll();
                    result.ArtProcessID = artProcessModel.ID;
                }
                //End  make Art Process


                //  make objectInk
                 var objectInk = await _repoObject.FindAll().FirstOrDefaultAsync(x => x.Name.ToUpper().Equals(scheduleDto.Object.ToUpper()) && x.ProcessID == process.ID);
                // var objectInk = await _repoObject.FindAll().FirstOrDefaultAsync(x => x.Name.Equals(scheduleDto.Process.ToUpper() == "STF" ? 2 : 1) && x.ArticleNoID == result.ArticleNoID);
                if (objectInk != null)
                {
                    result.ObjectInkID = objectInk.ID;
                }
                else
                {
                    // make objectInk
                    var objectInkModel = new InkTblObject { Name = scheduleDto.Object, ProcessID = process.ID };
                    _repoObject.Add(objectInkModel);
                    await _repoArtProcess.SaveAll();
                    result.ObjectInkID = objectInkModel.ID;
                }
                //End  objectInk


                //  make Part
                var Part = await _repoPart.FindAll().FirstOrDefaultAsync(x => x.Name.ToUpper().Equals(scheduleDto.Part.ToUpper()) && x.ProcessID == process.ID && x.ObjectID == result.ObjectInkID );
                // var objectInk = await _repoObject.FindAll().FirstOrDefaultAsync(x => x.Name.Equals(scheduleDto.Process.ToUpper() == "STF" ? 2 : 1) && x.ArticleNoID == result.ArticleNoID);
                if (Part != null)
                {
                    result.PartID = Part.ID;
                }
                else
                {
                    // make Part
                    var PartModel = new Part { Name = scheduleDto.Part, ObjectID = result.ObjectInkID , ProcessID = process.ID};
                    _repoPart.Add(PartModel);
                    await _repoPart.SaveAll();
                    result.PartID = PartModel.ID;
                }
                //End  Part

                result.CreatedBy = scheduleDto.CreatedBy;
                scope.Complete();
                return result;
            }
        }
        public async Task ImportExcel(List<ScheduleDtoForImportExcel> scheduleDtos)
        {
            try
            {
                var list = new List<ScheduleDto>();
                var listChuaAdd = new List<ScheduleDto>();
                var result = scheduleDtos.DistinctBy(x => new
                {
                    x.ModelName,
                    x.ModelNo,
                    x.ArticleNo,
                    x.Process,
                    x.Object,
                    x.Part,
                }).Where(x => x.ModelName != "").ToList();

                foreach (var item in result)
                {
                    var Schedule = await AddSchedule(item);
                    list.Add(Schedule);
                }

                var listAdd = new List<Schedules>();
                foreach (var schedule in list)
                {
                    if ( !await CheckExistSchedule(schedule))
                    {
                        var schedules = new Schedules();
                        schedules.ModelNameID = schedule.ModelNameID;
                        schedules.ModelNoID = schedule.ModelNoID;
                        schedules.ArticleNoID = schedule.ArticleNoID;
                        schedules.ArtProcessID = schedule.ArtProcessID;
                        schedules.InkTblObjectID = schedule.ObjectInkID;
                        schedules.PartID = schedule.PartID;
                        schedules.ProcessID = schedule.ProcessID;
                        schedules.CreatedBy = schedule.CreatedBy;
                        schedules.ApprovalBy = schedule.ApprovalBy;
                        if(schedule.ProcessID == 1) {
                            schedules.ProductionDate = Convert.ToDateTime(schedule.ProductionDate);
                            schedules.EstablishDate = Convert.ToDateTime(schedule.ProductionDate).AddDays(-30);
                        } else {
                            schedules.ProductionDate = Convert.ToDateTime(schedule.ProductionDate);
                            schedules.EstablishDate =  Convert.ToDateTime(schedule.ProductionDate).AddDays(-15);
                        }
                        schedules.CreatedDate = DateTime.Now;
                        schedules.UpdateTime = DateTime.Now;
                        schedules.ApprovalStatus = schedule.ApprovalStatus;
                        schedules.FinishedStatus = schedule.FinishedStatus;
                        schedules.Season = schedule.Season;
                        schedules.ModelName = null;
                        schedules.ModelNo = null;
                        schedules.ArticleNo = null;
                        _repoSchedule.Add(schedules);
                        await _repoSchedule.SaveAll();
                        listAdd.Add(schedules);
                       
                    }
                    else
                    {
                        listChuaAdd.Add(schedule);
                    }
                }
                var result1 = listAdd.Where(x => x.ID > 0).ToList();
                var result2 = listAdd.Where(x => x.ID == 0).ToList();
            }
            catch
            {
                throw;
            }
        }

        private async Task<bool> CheckExistSchedule(ScheduleDto schedule)
        {
            var data = await _repoSchedule.FindAll().AnyAsync(x => x.ModelNameID == schedule.ModelNameID && x.ModelNoID == schedule.ModelNoID && x.ArticleNoID == schedule.ArticleNoID && x.ArtProcessID == schedule.ArtProcessID && x.InkTblObjectID == schedule.ObjectInkID && x.PartID == schedule.PartID );
            return  data ;
        }
        public async Task<bool> AddRangeAsync(List<ChemicalForImportExcelDto> model)
        {
            var ingredients = _mapper.Map<List<Chemical>>(model);
            ingredients.ForEach(ingredient => { ingredient.isShow = true; });
            _repoChemical.AddRange(ingredients);
            return await _repoInk.SaveAll();
        }
        public async Task<bool> Add(ChemicalDto model)
        {
            var ink = _mapper.Map<Chemical>(model);
            ink.isShow = true;
            _repoChemical.Add(ink);
            return await _repoInk.SaveAll();
        }

        public Task<bool> Delete(object id)
        {
            throw new NotImplementedException();
        }

        public async Task<object> GetAllAsync()
        {
            // var consolidatedChildren =  from x in _repoSchedule
            //     .FindAll()
            //     group x by new
            //     {
            //         x.ModelNameID,
            //         x.ModelNoID,
            //         x.ArticleNoID,
            //         x.ArtProcessID,
            //         x.ObjectInkID
            //     } into gcs
            //     select new ScheduleGroupDto
            //     {
            //          Key = gcs,
            //         Schedules = gcs.ToList(),
            //     };

             var res = await _repoSchedule
                .FindAll().Include(x => x.ModelName)
                .Include(x => x.ModelNo)
                .Include(x => x.ArticleNo)
                .Include(x => x.Part)
                .Include(x => x.InkTblObject)
                .Include(x => x.ArtProcess).ThenInclude(x => x.Process)
                .Select(x => new {
                    ID = x.ID,
                    x.ModelNameID,
                    x.ModelNoID,
                    x.ArticleNoID,
                    x.ArtProcessID,
                    x.InkTblObjectID,
                    FinishedStatus = x.FinishedStatus,
                    ApprovalStatus = x.ApprovalStatus,
                    EstablishDate = x.EstablishDate,
                    ProductionDate = x.ProductionDate,
                    x.PartID,
                    ModelName = x.ModelName.Name,
                    ModelNo = x.ModelNo.Name,
                    ArticleNo = x.ArticleNo.Name,
                    ArtProcess = x.ArtProcess.Process.Name,
                    InkTblObject = x.InkTblObject.Name,
                    Part = x.Part.Name,

                })
              .ToListAsync();
              
              var items = res.GroupBy(x => new  {
                    x.ModelNameID,
                    x.ModelNoID,
                    x.ArticleNoID,
                    x.ArtProcessID,
                    x.InkTblObjectID,

                }).Select(x => new ScheduleGroupDto {
                    Key = x.Key,
                    ID = x.FirstOrDefault().ID,
                    Schedules = x.ToList(),
                    ModelName = x.FirstOrDefault().ModelName,
                    ModelNo = x.FirstOrDefault().ModelNo,
                    ArticleNo = x.FirstOrDefault().ArticleNo,
                    ArtProcess = x.FirstOrDefault().ArtProcess,
                    ObjectInk = x.FirstOrDefault().InkTblObject,
                    FinishedStatus = x.FirstOrDefault().FinishedStatus,
                    ApprovalStatus = x.FirstOrDefault().ApprovalStatus,
                    EstablishDate = x.FirstOrDefault().EstablishDate,
                    ProductionDate = x.FirstOrDefault().ProductionDate,
                    Part = x.ToList().Select(x => x.Part),

                }).OrderBy(x => x.EstablishDate);
                return  items;
        }

        public Task<bool> Add(ScheduleDto model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(ScheduleDto model)
        {
            throw new NotImplementedException();
        }

        public Task<PagedList<ScheduleDto>> GetWithPaginations(PaginationParams param)
        {
            throw new NotImplementedException();
        }

        public Task<PagedList<ScheduleDto>> Search(PaginationParams param, object text)
        {
            throw new NotImplementedException();
        }

        public ScheduleDto GetById(object id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ScheduleDetailDto>> GetDetailSchedule(int scheduleID)
        {
            var lists = await _repoSchedule.FindAll()
                .Include(x => x.ModelName)
                .Include(x => x.ModelNo)
                .Include(x => x.ArticleNo)
                .Include(x => x.Part)
                .Include(x => x.InkTblObject)
                .Include(x => x.ArtProcess).ThenInclude(x => x.Process)
                .Where(x => x.ID == scheduleID)
                .ProjectTo<ScheduleDetailDto>(_configMapper)
                .OrderByDescending(x => x.ID).Select(x => new ScheduleDetailDto
                {
                    ID = x.ID,
                    ModelNameID = x.ModelNameID,
                    ModelNoID  = x.ModelNoID,
                    ArticleNoID = x.ArticleNoID,
                    ArtProcessID = x.ArtProcessID,
                    InkTblObjectID = x.InkTblObjectID,
                    ObjectInk = x.ObjectInk,
                    ProcessID = x.ProcessID,
                    PartID = x.PartID,
                    ModelName = x.ModelName,
                    ModelNo = x.ModelNo,
                    Part = x.Part,
                    ArticleNo = x.ArticleNo,
                    ArtProcess = x.ArtProcess,
                    ApprovalStatus = x.ApprovalStatus,
                    FinishedStatus = x.FinishedStatus,
                    CreatedBy = x.CreatedBy,
                    ProductionDate = x.ProductionDate,
                    EstablishDate = x.EstablishDate,
                    Parts = _repoPart.FindAll().Where(y => y.ObjectID == x.InkTblObjectID && y.ProcessID == x.ProcessID).ToList(),
                    Supplier = _repoSup.FindAll().Where(y => y.ProcessID == x.ProcessID).ToList()
                }).ToListAsync();
            return lists;
        }

        Task<List<ScheduleDto>> IECService<ScheduleDto>.GetAllAsync()
        {
            throw new NotImplementedException();
        }
    }
}