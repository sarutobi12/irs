using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using INK_API.Helpers;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using INK_API._Repositories.Interface;
using INK_API._Services.Interface;
using INK_API.DTO;
using INK_API.Models;
using Microsoft.EntityFrameworkCore;

namespace INK_API._Services.Services
{
    public class SettingService : ISettingService
    {
        private readonly IMixingRepository _repoMixing;
        private readonly IMixingInfoRepository _repoMixingInfo;
        private readonly ISettingRepository _repoSetting;
        private readonly IStirRepository _repoStir;
        private readonly IMapper _mapper;
        public SettingService(IMixingInfoRepository repoMixingInfo, IStirRepository repoStir,IMixingRepository repoMixing , IMapper mapper , ISettingRepository repoSetting)
        {
            _repoMixing = repoMixing ;
            _repoSetting = repoSetting ;
            _repoMixingInfo = repoMixingInfo;
            _repoStir = repoStir;
            _mapper = mapper ;
        }

        public async Task<object> GetAllAsync()
        {
            return await _repoSetting.FindAll().ToListAsync();

        }

        public async Task<bool> Add(StirDTO model)
        {
            try
            {
                var stir = _mapper.Map<Stir>(model);
                _repoStir.Add(stir);
                return await _repoStir.SaveAll();
                
            }
            catch (System.Exception)
            {
                
                throw;
            }
        }
       
    }
}