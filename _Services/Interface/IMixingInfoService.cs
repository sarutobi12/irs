﻿using INK_API.DTO;
using INK_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INK_API._Services.Interface
{
    public interface IMixingInfoService
    {
        Task<MixingInfo> Mixing(MixingInfoForCreateDto mixing);
        Task<List<MixingInfoDto>> GetMixingInfoByGlueID(int glueID);
        Task<object> Stir(string glueName);
        Task<object> GetRPM(int mixingInfoID, string building, string startTime, string endTime);

    }
}
