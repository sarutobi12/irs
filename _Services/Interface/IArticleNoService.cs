﻿using INK_API.DTO;
using INK_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INK_API._Services.Interface
{
   public interface IArticleNoService : IECService<ArticleNoDto>
    {
        Task<List<ArticleNoDto>> GetArticleNoByModelNameID(int modelNameID);
        Task<List<ArticleNoDto>> GetArticleNoByModelNoID(int modelNoID);
    }
}
