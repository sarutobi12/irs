using System.Threading.Tasks;
using INK_API.DTO;
using INK_API.Helpers;
using INK_API.Models;

namespace INK_API._Services.Interface
{
    public interface IMakeGlueService
    {
        //Task<bool> CheckBrandExists(string brandId);
        Task<object> MakeGlue(string code);
        Task<object> GetAllGlues();
        Task<object> GetGlueWithIngredients(int glueid);
        Task<object> GetGlueWithIngredientByGlueCode(string code);
        Task<object> GetGlueWithIngredientByGlueID(int glueid);
        Task<object> GetGlueWithIngredientByGlueName(string glueName);
        object DeliveredHistory();
    }
}