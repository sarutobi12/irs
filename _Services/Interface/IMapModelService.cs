using System.Collections.Generic;
using System.Threading.Tasks;
using INK_API.DTO;
using INK_API.Helpers;
using INK_API.Models;

namespace INK_API._Services.Interface
{
    public interface IMapModelService : IECService<MapModelDto>
    {
        Task<List<ModelNoForMapModelDto>> GetModelNos(int modelNameID);
        Task<bool> MapMapModel(MapModel mapModel);
        Task<bool> Delete(int modelNameId, int modelNoId);
    }
}